package com.example.mergecontacts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DuplicateAdapter extends BaseAdapter {

	Context context;
	int resource, textViewResourceId;
	List<DuplicateContacts> list;
	Map<String, Integer> map = new HashMap<String, Integer>();
	String[] mKeys;

	public DuplicateAdapter(Map<String, Integer> map, Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.map = map;
		mKeys = map.keySet().toArray(new String[map.size()]);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return map.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return map.get(mKeys[position]);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = convertView;
		ViewHolder holder = null;
		if (view == null) {
			view = inflater.inflate(R.layout.contact_row, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		// Display Names and No. of Duplicates
		String key = mKeys[position];
		int value = (int) getItem(position);
		holder.nameView.setText(key.toString());
		holder.countView.setText(Integer.toString(value));
		return view;
	}

	class ViewHolder {

		TextView nameView, countView;

		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub
			nameView = (TextView) view.findViewById(R.id.name);
			countView = (TextView) view.findViewById(R.id.counts);
		}
	}
}
