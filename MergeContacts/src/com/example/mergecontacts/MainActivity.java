package com.example.mergecontacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity implements
		OnItemClickListener {

	ListView contactsView;

	List<String> contactData;

	Map<String, Integer> contact_map;
	Uri uri;
	DuplicateAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		uri = ContactsContract.Contacts.CONTENT_URI;
		contactsView = (ListView) findViewById(R.id.contacts_list);
		contact_map = new HashMap<String, Integer>();
		contactData = new ArrayList<String>();
		// getDuplicate();
		new DuplicateAsync().execute();
		// adapter = new DuplicateAdapter(contact_map, this);
		// contactsView.setAdapter(adapter);
		contactsView.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	class DuplicateAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(MainActivity.this);
			progress.setMessage("Just a moment please");
			progress.setCancelable(false);
			progress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			getDuplicate();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			adapter = new DuplicateAdapter(contact_map, getApplicationContext());
			contactsView.setAdapter(adapter);
			progress.dismiss();
		}

	}

	private void getDuplicate() {

		Cursor cursor = getContentResolver().query(uri, null, null, null,
				Phone.DISPLAY_NAME + " ASC");

		while (cursor.moveToNext()) {
			String name = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

			if (name == null)
				continue;
			String selection = ContactsContract.Contacts.DISPLAY_NAME + "=?";
			String[] selectionArgs = new String[] { name };

			if (!contact_map.containsKey(name)) {
				Cursor nameCursor = getContentResolver().query(uri, null,
						selection, selectionArgs, null);
				if (nameCursor.getCount() > 1) {
					contact_map.put(name, nameCursor.getCount());
				}
				nameCursor.close();
			}
		}
		cursor.close();

		for (Map.Entry<String, Integer> entry : contact_map.entrySet()) {
			contactData.add(entry.getKey());
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		String name = contactData.get(position);
		Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT)
				.show();
		Intent intent = new Intent(this, MergeActivity.class);
		intent.putExtra("name", name);
		startActivity(intent);
	}
}
