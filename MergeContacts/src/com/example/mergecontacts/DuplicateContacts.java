package com.example.mergecontacts;

public class DuplicateContacts {

	String name, organisation, id, lookUpKey;
	String[] phoneNo, email;
	boolean isChecked;

	public DuplicateContacts(String id, String loookUpKey, String name,
			String[] phoneNo, String[] email, String organisation,
			boolean isChecked) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.lookUpKey = loookUpKey;
		this.name = name;
		this.phoneNo = phoneNo;
		this.email = email;
		this.organisation = organisation;
		this.isChecked = isChecked;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLookUpKey() {
		return lookUpKey;
	}

	public void setLookUpKey(String lookUpKey) {
		this.lookUpKey = lookUpKey;
	}

	public String[] getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String[] phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String[] getEmail() {
		return email;
	}

	public void setEmail(String[] email) {
		this.email = email;
	}

}
