package com.example.mergecontacts;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class MergeActivity extends ActionBarActivity implements OnClickListener {

	ListView mergeContactsView;
	Button mergeButton;
	String name;
	Uri uri;
	List<DuplicateContacts> contactList;
	MergeAdapter adpater;
	List<Integer> position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merge);
		mergeButton = (Button) findViewById(R.id.mergeButton);
		mergeContactsView = (ListView) findViewById(R.id.mergeList);
		contactList = new ArrayList<DuplicateContacts>();
		name = getIntent().getStringExtra("name");
		uri = ContactsContract.Contacts.CONTENT_URI;
		// retriveContacts(name);
		new MergeAsync().execute();
		position = new ArrayList<Integer>();
		mergeButton.setOnClickListener(this);
	}

	private void retriveContacts(String contactName) {
		// TODO Auto-generated method stub
		String id, lookUpkey, organisation = null, title;
		String[] email;
		String[] phoneNo;
		int hasPhoneNo;
		String phNo;
		String emailAdd;
		String selection = ContactsContract.Contacts.DISPLAY_NAME + "=?";
		String[] selectionArgs = new String[] { contactName };
		Cursor cursor = getContentResolver().query(uri, null, selection,
				selectionArgs, null);

		while (cursor.moveToNext()) {
			id = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));
			lookUpkey = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));

			hasPhoneNo = cursor
					.getInt(cursor
							.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

			// RETRIVE PHONE NUMBER IF PRESENT
			int counter = 0;
			if (hasPhoneNo > 0) {
				Cursor phoneCursor = getContentResolver().query(
						Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?",
						new String[] { id }, null);

				phoneNo = new String[phoneCursor.getCount()];
				while (phoneCursor.moveToNext()) {
					phNo = phoneCursor.getString(phoneCursor
							.getColumnIndex(Phone.NUMBER));
					phoneNo[counter] = phNo;
					counter++;
				}
				phoneCursor.close();
			} else {
				phoneNo = new String[0];
			}

			// RETRIVE ORGANISATION OF THE PERSON IF ANY
			String selectOrg = ContactsContract.Data._ID + "= ? AND "
					+ ContactsContract.Data.MIMETYPE + "= ? ";
			String orgArgumets[] = new String[] {
					id,
					ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE };
			Cursor orgCusror = getContentResolver().query(
					ContactsContract.Data.CONTENT_URI, null, selectOrg,
					orgArgumets, null);
			while (orgCusror.moveToNext()) {

				organisation = orgCusror.getString(orgCusror
						.getColumnIndex(Organization.TITLE));

				title = orgCusror.getString(orgCusror
						.getColumnIndex(Organization.COMPANY));
			}
			orgCusror.close();
			counter = 0;
			// RETRIVE EMAIL OF THE PERSON
			Cursor emailCursor = getContentResolver().query(Email.CONTENT_URI,
					null, Email.CONTACT_ID + "=?", new String[] { id }, null);
			if (emailCursor.getCount() > 0) {
				email = new String[emailCursor.getCount()];

				while (emailCursor.moveToNext()) {
					emailAdd = emailCursor.getString(emailCursor
							.getColumnIndex(Email.DATA));
					email[counter] = emailAdd;
					counter++;
				}
			} else {
				email = new String[0];
			}
			emailCursor.close();
			contactList.add(new DuplicateContacts(id, lookUpkey, contactName,
					phoneNo, email, organisation, true));

		}
		cursor.close();
	}

	private void mergeContacts(List<Integer> mergePosition) {
		// TODO Auto-generated method stub
		int contactId = 0;

		ContentProviderResult[] result;
		for (int i = 0; i < mergePosition.size(); i++) {
			int position = mergePosition.get(i);
			String name = contactList.get(position).getName();
			String[] email = contactList.get(position).getEmail();
			String[] phNo = contactList.get(position).getPhoneNo();
			String organisation = contactList.get(position).getOrganisation();
			if (contactId == 0) {
				ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
				ops.add(ContentProviderOperation
						.newInsert(ContactsContract.RawContacts.CONTENT_URI)
						.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE,
								RawContacts.ACCOUNT_TYPE)
						.withValue(ContactsContract.RawContacts.ACCOUNT_NAME,
								RawContacts.ACCOUNT_NAME).build());

				ops.add(ContentProviderOperation
						.newInsert(Data.CONTENT_URI)
						.withValueBackReference(Data.RAW_CONTACT_ID, 0)
						.withValue(Data.MIMETYPE,
								StructuredName.CONTENT_ITEM_TYPE)
						.withValue(StructuredName.DISPLAY_NAME, name).build());

				for (int n = 0; n < phNo.length; n++) {

					ops.add(ContentProviderOperation
							.newInsert(Data.CONTENT_URI)
							.withValueBackReference(Data.RAW_CONTACT_ID, 0)
							.withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
							.withValue(Phone.NUMBER, phNo[n]).build());
				}

				for (int x = 0; x < email.length; x++) {
					ops.add(ContentProviderOperation
							.newInsert(Data.CONTENT_URI)
							.withValueBackReference(Data.RAW_CONTACT_ID, 0)
							.withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
							.withValue(Email.DATA, email[x]).build());
				}
				if (organisation != null) {
					ops.add(ContentProviderOperation
							.newInsert(Data.CONTENT_URI)
							.withValueBackReference(Data.RAW_CONTACT_ID, 0)
							.withValue(Data.MIMETYPE,
									Organization.CONTENT_ITEM_TYPE)
							.withValue(Organization.DATA, organisation).build());
				}
				try {
					result = getContentResolver().applyBatch(
							ContactsContract.AUTHORITY, ops);
					contactId = Integer.parseInt(result[0].uri
							.getLastPathSegment());
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OperationApplicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				ArrayList<ContentProviderOperation> operation = new ArrayList<ContentProviderOperation>();

				for (int j = 0; j < phNo.length; j++) {
					operation.add(ContentProviderOperation
							.newInsert(Data.CONTENT_URI)
							.withValue(Data.RAW_CONTACT_ID, contactId)
							.withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
							.withValue(Phone.NUMBER, phNo[j]).build());
				}

				for (int j = 0; j < email.length; j++) {
					operation.add(ContentProviderOperation
							.newInsert(Data.CONTENT_URI)
							.withValue(Data.RAW_CONTACT_ID, contactId)
							.withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
							.withValue(Email.DATA, email[j]).build());
				}
				if (organisation != null) {
					operation
							.add(ContentProviderOperation
									.newInsert(Data.CONTENT_URI)
									.withValue(Data.RAW_CONTACT_ID, contactId)
									.withValue(Data.MIMETYPE,
											Organization.CONTENT_ITEM_TYPE)
									.withValue(Organization.DATA, organisation)
									.build());
				}

				try {
					getContentResolver().applyBatch(ContactsContract.AUTHORITY,
							operation);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OperationApplicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		for (int i = 0; i < mergePosition.size(); i++) {
			int position = mergePosition.get(i);
			String loopUpKey = contactList.get(position).getLookUpKey();
			Uri uri = Uri.withAppendedPath(
					ContactsContract.Contacts.CONTENT_LOOKUP_URI, loopUpKey);
			getContentResolver().delete(uri, null, null);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		position.addAll(adpater.getMergePosition());
		// mergeContacts(position);
		new MergeContactsAsync().execute();

	}

	public class MergeAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(MergeActivity.this);
			progress.setMessage("Wait a Second");
			progress.setCancelable(false);
			progress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			retriveContacts(name);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			adpater = new MergeAdapter(getApplicationContext(),
					R.layout.activity_merge, R.id.name, contactList);
			mergeContactsView.setAdapter(adpater);
			progress.dismiss();
		}

	}

	class MergeContactsAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(MergeActivity.this);
			progress.setMessage("Merging Contacts ...");
			progress.setCancelable(false);
			progress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			mergeContacts(position);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progress.setMessage("Contacts Merge Successfully");
			progress.dismiss();
			startActivity(new Intent(getApplicationContext(),
					MainActivity.class));
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.merge, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
