package com.example.mergecontacts;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MergeAdapter extends ArrayAdapter<DuplicateContacts> {

	Context context;
	int resource;
	int textViewResourceId;
	List<DuplicateContacts> contacts;

	public MergeAdapter(Context context, int resource, int textViewResourceId,
			List<DuplicateContacts> contacts) {
		super(context, resource, textViewResourceId, contacts);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.resource = resource;
		this.textViewResourceId = textViewResourceId;
		this.contacts = contacts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder holder = null;
		if (view == null) {
			LayoutInflater inflator = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflator.inflate(R.layout.merge_row, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		String name = contacts.get(position).getName();
		String[] email = contacts.get(position).getEmail();
		String[] phoneNo = contacts.get(position).getPhoneNo();
		String organisation = contacts.get(position).getOrganisation();

		holder.nameView.setText(name);

		holder.orgLayout.removeAllViews();
		if (organisation != null) {
			TextView orgView = new TextView(context);
			orgView.setText(organisation.toString());
			orgView.setTextColor(Color.BLACK);
			holder.orgLayout.addView(orgView);
		}
		holder.phoneLayout.removeAllViews();
		for (int i = 0; i < phoneNo.length; i++) {
			TextView phoneView = new TextView(context);
			phoneView.setText(phoneNo[i]);
			phoneView.setTextColor(Color.BLACK);
			holder.phoneLayout.addView(phoneView);
		}
		holder.emailLayout.removeAllViews();
		for (int i = 0; i < email.length; i++) {
			TextView emailView = new TextView(context);
			emailView.setText(email[i]);
			emailView.setTextColor(Color.BLACK);
			holder.emailLayout.addView(emailView);
		}

		holder.conditionView.setTag(position);
		holder.conditionView.setChecked(contacts.get(position).isChecked());
		holder.conditionView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int position = (int) v.getTag();
				contacts.get(position).setChecked(
						((CompoundButton) v).isChecked());
				Log.i("Name", contacts.get(position).getName());
			}
		});
		return view;
	}

	class ViewHolder {

		TextView nameView;
		LinearLayout phoneLayout, emailLayout, orgLayout;
		CheckBox conditionView;

		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub
			nameView = (TextView) view.findViewById(R.id.contact_name);
			conditionView = (CheckBox) view.findViewById(R.id.condion_check);
			phoneLayout = (LinearLayout) view.findViewById(R.id.phone_layout);
			emailLayout = (LinearLayout) view.findViewById(R.id.email_layout);
			orgLayout = (LinearLayout) view.findViewById(R.id.org_layout);
		}
	}

	public List<Integer> getMergePosition() {

		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < contacts.size(); i++) {
			boolean check = contacts.get(i).isChecked();
			if (check) {
				list.add(i);
			}
		}
		return list;
	}

}
